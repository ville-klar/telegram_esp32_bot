# 🚀 Telegram bot ESP32 emoji display 📠


ESP32 powered emoji display.
Based (primarily) on 

- [Universal-Arduino-Telegram-Bot](https://github.com/witnessmenow/Universal-Arduino-Telegram-Bot)
- [Adafruit_ILI9341](https://github.com/adafruit/Adafruit_ILI9341) and
- [Adafruit_ImageReader](https://github.com/adafruit/Adafruit_ImageReader)

See my [blogpost](https://villeklar.com/post/2020/08/17/telegram-emoji-display/) for further information.
The dev environment is [platformio](https://platformio.org/) which I highly recommend.

# Schematic 

![schematic](https://villeklar.com/post/Telegram_Emoji_Display/schematic_min.svg)

# Installation

1. Unzip the bmp.zip and put it on a SD card. Alternativley, produce your own. See [blogpost](https://villeklar.com/post/2020/08/17/telegram-emoji-display/) for further information.
2. Install the libraries using

```bash
platformio lib install 1262 64 13 571 5891
```
The SD card library that is installed with the _ImageReader_ library is not compatible with the esp32.
The esp32 environment has its own implementation of the SD library so we can just remove the SD library downloaded by platformio.

```bash
rm -r .pio/libdeps/wemos_d1_mini32/SD
```

3. Compile and upload using 

```bash
make
```
and 

```bash
make upload
```
You can also upload and open the serial port interface afterwards with

```bash
make monitor
```




