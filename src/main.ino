#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>         // Core graphics library
#include <Adafruit_ILI9341.h>     // Hardware-specific library
#include <SdFat.h>                // SD card & FAT filesystem library
#include <Adafruit_SPIFlash.h>    // SPI / QSPI flash library
#include <Adafruit_ImageReader.h> // Image-reading functions
#include <WiFi.h>
#include <WiFiClientSecure.h>

#include <UniversalTelegramBot.h>
#include <ArduinoJson.h>

#include <Fonts/FreeSans18pt7b.h>
#include <Fonts/FreeSans12pt7b.h>

#include "wifikeys.h"           // This is a gitignored file containing the wifi credentials and telegram bot token
#include "emojis.h"

#define USE_SD_CARD
#define SD_CS   27 // SD card select pin
#define TFT_CS   5      
#define TFT_DC   26       
#define TFT_RST  33 

Adafruit_ILI9341 tft = Adafruit_ILI9341(TFT_CS, TFT_DC,TFT_RST);

SdFat                SD;         // SD card filesystem
Adafruit_ImageReader reader(SD); // Image-reader object, pass in SD filesys
Adafruit_Image       img;        // An image loaded into RAM
int32_t              width  = 0, // BMP image dimensions
                     height = 0;


WiFiClientSecure client;
UniversalTelegramBot bot(BOTtoken, client);

int botRequestDelay = 1000;
unsigned long lastTimeBotRan;

void setup() {
    ImageReturnCode stat; // Status from image-reading functions
    Serial.begin(115200);
    tft.begin();          // Initialize screen
    tft.fillScreen(ILI9341_WHITE);
    tft.setFont(&FreeSans12pt7b);
    tft.setCursor(0, 20);
    tft.setTextColor(ILI9341_BLACK);  
    tft.setTextSize(1);
    tft.println(F("Initializing filesystem..."));

    if(!SD.begin(SD_CS, SD_SCK_MHZ(25))) { // ESP32 requires 25 MHz limit
        Serial.println(F("SD begin() failed"));
        for(;;); // Fatal error, do not continue
    }

    Serial.println(F("OK!"));

    // Attempt to connect to Wifi network:
    Serial.print("Connecting Wifi: ");
    Serial.println(ssid);
    tft.println("Connecting Wifi");

    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
        Serial.print(".");
        tft.print(".");
        delay(500);
    }

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());

    tft.setCursor(0, 130);
    tft.println("Connected!");
    tft.println("Ready to receive emoji");

    tft.setFont(&FreeSans18pt7b);
    tft.setTextColor(ILI9341_BLACK);  
    tft.setTextSize(1);
}


void loop() {
    if (millis() > lastTimeBotRan + botRequestDelay)  {
        int numNewMessages = bot.getUpdates(bot.last_message_received + 1);
        while(numNewMessages) {
            for (int i=0; i<numNewMessages; i++) {
                bot.sendMessage(bot.messages[i].chat_id, "Message received", "");
                String sender = bot.messages[i].from_name;
                String msg = bot.messages[i].text;
                Serial.print("message: ");
                Serial.print(msg);
                Serial.print("from ");
                Serial.println(sender);
                bool found = false;
                for (i=0;i<NUM_EMOJI;i++) {
                    if (msg == Emoji[i].emoji){
                        found = true;
                        char * path_buf  = strdup(Emoji[i].path);
                        Serial.println(path_buf);
                        tft.fillScreen(ILI9341_WHITE);          
                        reader.drawBMP(path_buf, tft, 0, 0);
                        tft.setCursor(0, 280);
                        tft.print("From: ");
                        tft.println(sender);
                    }
                }
                if (!found) {
                    tft.setFont(&FreeSans12pt7b);
                    tft.fillScreen(ILI9341_WHITE);          
                    tft.setCursor(20, 160);
                    tft.println("Emoji not found");
                    tft.println("or incompatible message");
                    tft.setFont(&FreeSans18pt7b);
                }
                }
            numNewMessages = bot.getUpdates(bot.last_message_received + 1);
        }
        lastTimeBotRan = millis();
    }
}

