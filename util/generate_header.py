#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys

def main():
    try:
        infile = open(sys.argv[1], 'r',encoding="utf-8")
    except IndexError:
        print("ERROR:Provide emoji list file as argument")
        sys.exit()

    translate = lambda e: '-'.join('%x'%ord(c) for c in e)

    with open("emojis.h", 'w', encoding="utf-8") as outfile:
        outfile.write('#pragma once\n                      \n\n')               # Leave some space here since we'll seek it later to insert the NUM_EMOJI macro
        outfile.write('typedef struct\n{\tchar emoji[29];\n\tchar path[48];\n}EmojiLUT;\n\n')
        outfile.write('const EmojiLUT Emoji[NUM_EMOJI] = {\n')
        numLines = 0
        prevcp = ""
        for line in infile.read().splitlines():
            codepoint = translate(line).replace("-fe0f","")
            if prevcp == codepoint:
                pass 
            else:
                outfile.write('{"'+line+'","'+codepoint+'.bmp"},\n')
                numLines += 1
            prevcp = codepoint
        outfile.write('\n};')
        outfile.seek(13)                            # Go to the previously allocated whitespace to enter the NUM_EMOJI macro
        outfile.write('#define NUM_EMOJI '+str(numLines))

    infile.close()



if __name__ == "__main__":
    main()


