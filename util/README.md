# Utilities to generate graphics

Use the provided python script to produce the required emojis header.

```bash
python generate_header.py emojiList.txt
```

Unzip and place the bitmaps in the root of your SD card.

## Emoji graphics

The emoji graphics in **bmp.zip** are derived from

- [noto-emoji](https://github.com/googlefonts/noto-emoji) licensed under Apache version 2.0 http://www.apache.org/licenses/LICENSE-2.0.html
- [twemoji](https://github.com/twitter/twemoji) licensed under CC-BY 4.0: https://creativecommons.org/licenses/by/4.0/


